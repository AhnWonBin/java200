package part1;

import java.util.Calendar;
import java.util.Date;

/**
 * part 1 - 006
 *
 * Created on 2019.02.01<br>
 * calendar 클래스를 이용한 날짜 구하기<br>
 * java.util.Date 를 이용해 하루 전과 하루 뒤를 구함<br>
 * @author WonBin
 * 
 * 
 * - 주석 사용의 예
 * @author 문서 작성자
 * @version 문서의 버전
 * @see 추가 또는 관련 내용 참고
 * example)
 * 			@see {@link java.util.Vector}
 * @since 언제부터 사용했는가 @since version 1.1
 * @param argument  @param int 돈액수 입력
 * @return 리턴 내용  @return 입력된 액수만큼의 사탕
 * 
 * 
 * API 문서 만들기
 * - 주석은 html 문서로 작성되므로 html 에서 사용하는 태그를 그대로 사용할 수 있다.
 * - 문서 주석을 만드는 작업이 끝나면 문서로 변형
 * - 문서 생성할 파일 여기서는 CalendarAfAndBf.java 를 임의 디렉토리에 복사한다.
 * - 복사한 java 파일 경로로 가서 cmd 상에서 명령어를 입력한다.
 * javadoc-use* .java  또는
 * javadoc CalendarAfAndBf.java 를 입력한다.
 * 
 * Encoding Error 발생 시 해당 OS 맞는 인코딩으로 변경 후 명령어를 입력한다.
 * 
 */
public class CalendarAfAndBf {

	/**
	 * @param d 입력 다음 날을 출력하기 위한 입력 날
	 * @return 하루 뒤를 출력
	 */
	public Date afterOneDay(Date d){
		// d 기준 날짜를 입력한다.
		long dd = d.getTime();
		// 밀리세컨드 * 60초 * 60분 * 24시간 == 하루
		return new Date(dd + 1000 * 60 * 60 * 24);
	}
	
	/**
	 * @param d 입력 전 날을 출력하기 위한 입력 날
	 * @return 하루 전을 출력
	 */
	public Date beforeOneDay(Date d){
		// d 기준 날짜를 입력한다.
		long dd = d.getTime();
		return new Date(dd - 1000 * 60 * 60 * 24);
	}
	
	/*
	public Date setDate(int year, int month, int day){
		Calendar calendar = Calendar.getInstance();
		// 0 ~ 11 까지 존재하기 때문에 - 1
		calendar.set(year, month - 1, day);
		return new Date(calendar.getTimeInMillis());
	}
	*/
	
}
