package part1;

/**
 *  part 1 - 004
 *
 */
public class Hello {
	
	/**
	 * public static void main(String[] args) 는 프로그램의 시작점이며 main 메소드이다.
	 * 프로그램이 실행될 때 가장 먼저 main을 찾으며,
	 * main이 없다면 Exception in thread "main" java.lang.NoSuchMethodError: main 에러가 발생한다. 
	 * 
	 */
	public static void main(String[] args) {
		String str = "안녕하세요 JAVA를 즐깁시다!";
		System.out.println(str);
	}
	
}
